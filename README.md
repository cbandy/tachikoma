## Install some JAR files using [Ivy](http://ant.apache.org/ivy/):

### OS X

```sh
brew install ivy
cd ~/this/repository
ivy -retrieve "jars/[artifact](-[revision]).[ext]"
```

### Ubuntu

```sh
sudo apt-get install ivy
cd ~/this/repository
java -jar /usr/share/java/ivy.jar -retrieve "jars/[artifact](-[revision]).[ext]"
```


## Run migrations

```sh
cd ~/this/repository
./flyway -url='jdbc:postgresql://localhost/sample?user=sample&password=samplepass' migrate
```


## Interrogate deployed version

```sh
psql -Atc 'SELECT sc_version()' 'postgresql://localhost/sample?user=sample&password=samplepass'
```

```sh
cd ~/this/repository
./flyway -url='jdbc:postgresql://localhost/sample?user=sample&password=samplepass' info
```
