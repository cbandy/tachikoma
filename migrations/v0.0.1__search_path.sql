SET search_path = 'tachikoma';

DO $$
BEGIN
    EXECUTE format('ALTER ROLE %I SET search_path FROM CURRENT', current_user);
END;
$$;


CREATE FUNCTION sc_version()
RETURNS text AS $$

    SELECT version FROM schema_version ORDER BY version_rank DESC LIMIT 1;

$$ LANGUAGE SQL IMMUTABLE STRICT;

COMMENT ON FUNCTION sc_version() IS
'FIXME';


-- Verify

SELECT 1 / char_length(sc_version());
